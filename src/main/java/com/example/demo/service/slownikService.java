package com.example.demo.service;

import com.example.demo.model.slownik;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class slownikService
{


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private com.example.demo.repository.slownikRepository slownikRepository;

    public List<slownik> getAll()
    {
        return this.slownikRepository.findAll();
    }

    public slownik getOne( Integer id )
    {
        slownik jedenSlownik = this.slownikRepository.findById(id).get();

        return jedenSlownik;
    }

    public slownik getOneLazyTest1( Integer id )
    {
        Session sessionLazy = null;
        if (entityManager == null
                || (sessionLazy = entityManager.unwrap(Session.class)) == null) {

            throw new NullPointerException();
        }

        slownik jedenSlownik = (slownik) sessionLazy.createQuery("From slownik where id = "+id+"").uniqueResult();
        return jedenSlownik;
    }

    public slownik getOneLazyTest2( Integer id )
    {
        Session sessionLazy = null;
        if (entityManager == null
                || (sessionLazy = entityManager.unwrap(Session.class)) == null) {

            throw new NullPointerException();
        }



            slownik jedenSlownik = sessionLazy.find(slownik.class, id);
            System.out.println(jedenSlownik);
            return jedenSlownik;


    }

}
