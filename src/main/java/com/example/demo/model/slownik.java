package com.example.demo.model;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
public class slownik implements Serializable
{
    @Id
    @Column
    private int id;

    @Column
    private String title;

    @OneToMany( fetch = FetchType.LAZY )
    @JoinColumn(name = "id_fk")
    public List<slownik> slownikiList; // public tylko dla testów


}
