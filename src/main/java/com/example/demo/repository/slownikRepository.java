package com.example.demo.repository;


import com.example.demo.model.slownik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface slownikRepository extends JpaRepository<slownik , Integer>
{

}
