package com.example.demo.controller;

import com.example.demo.model.slownik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.slownikService;

import java.util.List;
import java.util.Optional;

@RestController
public class slownikController
{
    @Autowired
    private slownikService slownikService;

    @GetMapping("/")
    public List<slownik> getSlownikAll()
    {
        return this.slownikService.getAll();
    }

    @GetMapping("/{id}")
    public slownik getOne(@PathVariable("id") Integer id )
    {
        return this.slownikService.getOne(id);
    }

    @GetMapping("/lazy-test-1/{id}")
    public slownik getOneLAzyTest1(@PathVariable("id") Integer id )
    {
        return this.slownikService.getOneLazyTest1(id);
    }

    @GetMapping("/lazy-test-2/{id}")
    public slownik getOneLAzyTest2(@PathVariable("id") Integer id )
    {
        return this.slownikService.getOneLazyTest2(id);
    }
}
