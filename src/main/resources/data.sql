CREATE TABLE slownik
(
     id                  INT NOT NULL PRIMARY KEY
   , id_fk               INT NULL
   , title               VARCHAR(50) NOT NULL
   , foreign key (id) references slownik(id)
);

INSERT INTO slownik( id, id_fk, title ) values ( 1, null, 'start' );
INSERT INTO slownik( id, id_fk, title ) values ( 2, 1, 'level 1-1' );
INSERT INTO slownik( id, id_fk, title ) values ( 3, 1, 'level 1-2' );
INSERT INTO slownik( id, id_fk, title ) values ( 4, 1, 'level 1-3' );
INSERT INTO slownik( id, id_fk, title ) values ( 5, 4, 'level 2-1' );
INSERT INTO slownik( id, id_fk, title ) values ( 6, 4, 'level 2-2' );
INSERT INTO slownik( id, id_fk, title ) values ( 7, 6, 'level 3-1' );
INSERT INTO slownik( id, id_fk, title ) values ( 8, 7, 'level 4-1' );
INSERT INTO slownik( id, id_fk, title ) values ( 9, 8, 'level 5-1' );