package com.example.demo;

import com.example.demo.model.slownik;
import com.example.demo.service.slownikService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Optional;

@SpringBootTest
class DemoApplicationTests
{

	@Autowired
	private com.example.demo.service.slownikService slownikService;

	@Transactional
	@Test()
	void ShouldLazyLoadData1()
	{
		slownik tempSlownik = this.slownikService.getOne(8);
		System.out.println("tempSlownik=>"+tempSlownik);

		Assert.isNull(tempSlownik.slownikiList , "Pobranie zwróciło podrzędne elementy a nie powinno!");
	}

	@Transactional
	@Test()
	void ShouldLazyLoadData2()
	{
		slownik tempSlownik = this.slownikService.getOneLazyTest1(8);
		System.out.println("tempSlownik=>"+tempSlownik);

		Assert.isNull(tempSlownik.slownikiList , "Pobranie zwróciło podrzędne elementy a nie powinno!");
	}

	@Transactional
	@Test()
	void ShouldLazyLoadData3()
	{
		slownik tempSlownik = this.slownikService.getOneLazyTest2(8);
		System.out.println("tempSlownik=>"+tempSlownik);

		Assert.isNull(tempSlownik.slownikiList , "Pobranie zwróciło podrzędne elementy a nie powinno!");
	}
}
